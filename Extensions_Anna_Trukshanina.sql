--To install an extension
CREATE EXTENSION pg_stat_statements;

CREATE EXTENSION pgcrypto;

--Listing Installed Extensions:
SELECT * FROM pg_extension;

--Create a new table called "employees
CREATE TABLE employees (
   id serial PRIMARY KEY,
   first_name VARCHAR(255),
   last_name VARCHAR(255),
   email VARCHAR(255),
   encrypted_password TEXT
);

--Insert sample employee data into the table. 
INSERT INTO employees (first_name, last_name, email, encrypted_password) VALUES
   ('Polins', 'Grin', 'polina.grin@example.com', crypt('12345', gen_salt('bf'))),
   ('Anna', 'Trukshanina', 'anna.trukshanina@example.com', crypt('56789', gen_salt('bf'))),
   ('Dasha', 'Delbano', 'dasha.delbano@example.com', crypt('2024', gen_salt('bf')));
     
SELECT * FROM employees;

--Update an employee's personal information, such as their last name
UPDATE employees SET last_name = 'RedBull' WHERE email = 'polina.grin@example.com';

SELECT * FROM employees;

-- Delete an employee record using the email column
DELETE FROM employees WHERE email = 'anna.trukshanina@example.com';

SELECT * FROM employees;

--Configure the pg_stat_statements extension
ALTER SYSTEM SET shared_preload_libraries TO 'pg_stat_statements';

ALTER SYSTEM SET pg_stat_statements.track TO 'all';

--Run the following query to gather statistics for the executed statements:
SELECT * FROM pg_stat_statements;

--- Analyze the output of the pg_stat_statements view (self-check)

--Identify the most frequently executed queries
SELECT calls, query
FROM pg_stat_statements
ORDER BY  calls  DESC

-- Determine which queries have the highest average and total runtime

SELECT calls, 
       total_exec_time, 
       rows, 
       total_exec_time / calls AS avg_time,
	   query
FROM pg_stat_statements 
ORDER BY calls DESC